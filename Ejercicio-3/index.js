function binaryConvert(num) {
    let bin = '';

    while (num > 0) {
        bin = (num % 2) + bin;
        num = Math.floor(num / 2);
    }

    return bin;
}

console.log(binaryConvert(5));
