'use strict';

const getUsers = async (limit) => {
    try {
        const response = await fetch(
            `https://randomuser.me/api/?results=${limit}`
        );

        const { results } = await response.json();

        for (const user of results) {
            const userName = user.login.username;
            const { first, last } = user.name;
            const gender = user.gender;
            const country = user.location.country;
            const email = user.email;
            const profilePic = user.picture.medium;

            const usersArray = [
                userName,
                first,
                last,
                gender,
                country,
                email,
                profilePic,
            ];

            console.log(usersArray);
        }
    } catch (error) {
        console.log(error);
    }
};

getUsers(2);
